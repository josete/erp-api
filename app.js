var express = require('express');
var bodyParser = require("body-parser");
const { Pool, Client } = require('pg');
var async = require("async");

var app = express();
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());

const pool = new Pool({
    user: 'erp',
    host: '127.0.0.1',
    database: 'erp',
    password: 'erp',
    port: '5432',
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.get('/orders', (request, reply) => {
    async.waterfall([(callback) => {
        pool.query('select * from orders;', (err, res) => {
            callback(null, res.rows);
        });
    }, (rows, callback) => {
        var productsorder = [];
        async.each(rows, (row, callback) => {
            pool.query('select p.id,p.name,p.reference,po.qty from products p join productsorder po on p.id=po.productid where po.orderid=\'' + row.orderid + '\';', (err, res) => {
                productsorder.push(res.rows);
                callback();
            });
        }, function (err) {
            var i = 0;
            async.each(rows, (row, callback) => {                
                row.products = productsorder[i];
                i++;
                callback();
            }, function (err) {
                callback(null, rows);
            });
        });
    }], function (err, results) {
        reply.status(200);
        reply.send({ orders: results });
    });
});

app.get('/orders/month', (request, reply) => {
    async.parallel([(callback) => {
        pool.query('select count(*) as total from orders where extract(month from date) = extract(month from now());', (err, res) => {
            callback(null, res.rows[0].total);
        });
    }, (callback) => {
        pool.query('select count(*) as nosent from orders where extract(month from date) = extract(month from now()) and sent = false;', (err, res) => {
            callback(null, res.rows[0].nosent);
        });
    }, (callback) => {
        pool.query('select count(*) as sent from orders where extract(month from date) = extract(month from now()) and sent = true;', (err, res) => {
            callback(null, res.rows[0].sent);
        });
    }], function (err, results) {
        if (!err) {
            var orderQty = {};
            orderQty.total = results[0];
            orderQty.nosent = results[1];
            orderQty.sent = results[2];
            reply.status(200);
            reply.send(orderQty);
        }
    });
});

app.get('/orders/month/:month', (request, reply) => {
    var month = request.params.month;
    async.parallel([(callback) => {
        pool.query('select count(*) as total from orders where extract(month from date) = ' + month + ';', (err, res) => {
            callback(null, res.rows[0].total);
        });
    }, (callback) => {
        pool.query('select count(*) as nosent from orders where extract(month from date) = ' + month + ' and sent = false;', (err, res) => {
            callback(null, res.rows[0].nosent);
        });
    }, (callback) => {
        pool.query('select count(*) as sent from orders where extract(month from date) = ' + month + ' and sent = true;', (err, res) => {
            callback(null, res.rows[0].sent);
        });
    }], function (err, results) {
        if (!err) {
            var orderQty = {};
            orderQty.total = results[0];
            orderQty.nosent = results[1];
            orderQty.sent = results[2];
            reply.status(200);
            reply.send(orderQty);
        }
    });
});

app.get('/orders/graph', (request, reply) => {
    var months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    async.waterfall([(callback) => {
        pool.query('select extract(month from date) as month, count(*) as total from orders where extract(year from date) = extract(year from now()) group by extract(month from date);', (err, res) => {
            callback(null, res.rows);
        });
    }, (orders, callback) => {
        var ordersMonth = [];
        async.each(orders, (order, callback) => {
            ordersMonth.push({ month: months[order.month - 1], total: order.total });
            callback();
        }, function (err) {
            if (!err) {
                callback(null, ordersMonth);
            }
        });
    }], function (err, results) {
        if(!err){
            reply.status(200);
            reply.send({ordersGraph:results});
        }
    })
});

app.get('/orders/map', (request, reply) => {    
    async.waterfall([(callback) => {
        pool.query('select count(*),lat,lon,pc from orders where extract(year from date) = extract(year from now()) group by lat,lon,pc;', (err, res) => {
            callback(null, res.rows);            
        });
    }, (ordersPos, callback) => {
        callback(null,ordersPos);
    }], function (err, results) {
        if(!err){
            reply.status(200);
            reply.send({ordersMap:results});
        }
    })
});

app.get('/warehouse', (request, reply) => {
    async.waterfall([(callback) => {
        pool.query('select p.name,p.description,p.price,p.reference,p.image,w.quantity from products p join warehouse w on p.id=w.productid;', (err, res) => {
            callback(null, res.rows);            
        });
    }, (ordersPos, callback) => {
        callback(null,ordersPos);
    }], function (err, results) {
        if(!err){
            reply.status(200);
            reply.send({products:results});
        }
    })
});

app.post('/warehouse',(request,reply)=>{
    var newProduct = {};
    async.waterfall([(callback) => {
        pool.query('insert into products(name,description,image,price,reference) values($1,$2,$3,$4,$5) returning *',
            [request.body.name,request.body.description,request.body.image,request.body.price,request.body.reference],
            (err, res) => {
                newProduct = {name:res.rows[0].name,description:res.rows[0].description,price:res.rows[0].price,reference:res.rows[0].reference,image:res.rows[0].image};
                callback(null, res.rows[0].id);
        });
    }, (idProducto, callback) => {
        pool.query('insert into warehouse(productid,quantity) values($1,$2) returning *',
            [idProducto,request.body.quantity],(err, res) => {
                newProduct['quantity'] = res.rows[0].quantity;
                callback(null, res.rows[0].id);
        });
    }], function (err, results) {
        if(!err){
            reply.status(200);
            reply.send(newProduct);
        }
    })
});

app.listen(3001);