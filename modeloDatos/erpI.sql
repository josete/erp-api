-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.1
-- PostgreSQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- object: erp | type: ROLE --
-- DROP ROLE IF EXISTS erp;
CREATE ROLE erp WITH 
	SUPERUSER
	ENCRYPTED PASSWORD 'erp';
-- ddl-end --


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: new_database | type: DATABASE --
-- -- DROP DATABASE IF EXISTS new_database;
-- CREATE DATABASE new_database;
-- -- ddl-end --
-- 

-- object: public."Orders" | type: TABLE --
-- DROP TABLE IF EXISTS public."Orders" CASCADE;
CREATE TABLE public."Orders"(
	"orderId" text NOT NULL,
	name text,
	surname text,
	address text,
	city text,
	country text,
	province text,
	pc numeric,
	tel numeric,
	date date,
	sent bool,
	email text,
	lat float,
	lon float,
	CONSTRAINT "Orders_pk" PRIMARY KEY ("orderId")

);
-- ddl-end --
ALTER TABLE public."Orders" OWNER TO erp;
-- ddl-end --

-- object: public."Products" | type: TABLE --
-- DROP TABLE IF EXISTS public."Products" CASCADE;
CREATE TABLE public."Products"(
	id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	name text,
	description text,
	image text,
	price integer,
	reference text,
	CONSTRAINT products_pk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public."Products" OWNER TO erp;
-- ddl-end --

-- object: public."ProductsOrder" | type: TABLE --
-- DROP TABLE IF EXISTS public."ProductsOrder" CASCADE;
CREATE TABLE public."ProductsOrder"(
	id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	"orderId" text,
	"productId" integer,
	qty integer,
	CONSTRAINT "ProductsOrder_pk" PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public."ProductsOrder" OWNER TO erp;
-- ddl-end --

-- object: public."Emails" | type: TABLE --
-- DROP TABLE IF EXISTS public."Emails" CASCADE;
CREATE TABLE public."Emails"(
	id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
	email text NOT NULL,
	subscribed bool,
	CONSTRAINT "Emails_pk" PRIMARY KEY (id),
	CONSTRAINT "emailUnique" UNIQUE (email)

);
-- ddl-end --
ALTER TABLE public."Emails" OWNER TO erp;
-- ddl-end --

-- object: public."Warehouse" | type: TABLE --
-- DROP TABLE IF EXISTS public."Warehouse" CASCADE;
CREATE TABLE public."Warehouse"(
	id smallint NOT NULL GENERATED ALWAYS AS IDENTITY ,
	"productId" smallint,
	quantity integer,
	CONSTRAINT "Warehouse_pk" PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public."Warehouse" OWNER TO postgres;
-- ddl-end --

-- object: "fkOrderId" | type: CONSTRAINT --
-- ALTER TABLE public."ProductsOrder" DROP CONSTRAINT IF EXISTS "fkOrderId" CASCADE;
ALTER TABLE public."ProductsOrder" ADD CONSTRAINT "fkOrderId" FOREIGN KEY ("orderId")
REFERENCES public."Orders" ("orderId") MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: "fkProductId" | type: CONSTRAINT --
-- ALTER TABLE public."ProductsOrder" DROP CONSTRAINT IF EXISTS "fkProductId" CASCADE;
ALTER TABLE public."ProductsOrder" ADD CONSTRAINT "fkProductId" FOREIGN KEY ("productId")
REFERENCES public."Products" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: "fkProducts" | type: CONSTRAINT --
-- ALTER TABLE public."Warehouse" DROP CONSTRAINT IF EXISTS "fkProducts" CASCADE;
ALTER TABLE public."Warehouse" ADD CONSTRAINT "fkProducts" FOREIGN KEY ("productId")
REFERENCES public."Products" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


